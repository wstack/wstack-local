'use strict'

const START_TASK = 'START_TASK'
const FINISH_TASK = 'FINISH_TASK'
const SKIP_FILE = 'SKIP_FILE'
const TASK_FAILED = 'TASK_FAILED'

module.exports = {
  START_TASK,
  FINISH_TASK,
  SKIP_FILE,
  TASK_FAILED,
  startTask: (source, params) => { return { type: START_TASK, source, params } },
  finishTask: (source) => { return { type: FINISH_TASK, source } },
  skipFile: (source, params) => { return { type: SKIP_FILE, source, params } },
  taskFailed: (source, error) => { return { type: TASK_FAILED, source, error } }
}
