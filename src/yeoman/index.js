/**
 * Wrapper for yeoman.
 */

'use strict'

const Promise = require('bluebird')
const yeoman = require('yeoman-environment')

const NS = 'wysiwygoy:wordpress'

module.exports = {
  generate: (generator, directory, projectModel, options) => {
    process.chdir(directory)

    const env = Promise.promisifyAll(yeoman.createEnv())
    env.register(require.resolve(generator), NS)
    return env.runAsync(`${NS} ${projectModel}`, options)
  }
}
