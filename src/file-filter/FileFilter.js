'use strict'

const FileFilterRule = require('./FileFilterRule')

class FileFilter {
  constructor (rules = []) {
    this.rules = rules
  }

  static fromString (rulesString) {
    if (!rulesString) {
      rulesString = ''
    }
    return new FileFilter(rulesString.split('\n').map(FileFilterRule.fromString).filter(v => v !== null))
  }

  includes (fileName) {
    for (let rule of this.rules) {
      if (rule.includes(fileName)) {
        return true
      }
      if (rule.excludes(fileName)) {
        return false
      }
    }
    return true
  }
}

module.exports = FileFilter
