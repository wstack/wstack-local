'use strict'

const projectController = require('./project.controller')

module.exports = {
  createFromScratch: projectController.createFromScratch,
  createFromExistingSite: projectController.createFromExistingSite
}
