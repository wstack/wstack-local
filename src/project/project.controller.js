'use strict'

const Writable = require('stream').Writable

const Project = require('./Project')

module.exports = {
  createFromScratch: createFromScratch,
  createFromExistingSite: createFromExistingSite
}

/**
 * Given the environment settings, returns a handler function
 * that creates a project in its own directory under the work directory
 * specified in the environment.
 *
 * @param environment environment for the handler
 * @returns {Function} handler function
 */
function createFromScratch (environment) {
  return function (req, res) {
    res.writeHead(200, { 'Content-Type': 'application/json' })
    res.write(Buffer.alloc(1024, 32))
    res.write('[\n')

    const output = Writable({ objectMode: true })
    output._write = (chunk, enc, next) => {
      res.write(JSON.stringify(chunk))
      res.write(',\n')
      next()
    }

    Project
      .createFromScratch(
        environment,
        req.body.slug,
        req.body.adminUser,
        req.body.adminPassword,
        req.body.adminEmail,
        req.body.deployHost,
        req.body.deployUser,
        output
      )
      .then((project) => {
        res.write('\n]\n')
      })
      .catch((err) => {
        console.log(err)
        res.write('\n,' + JSON.stringify(err) + ']\n')
      })
      .finally(() => {
        res.end()
      })
  }
}

/**
 * Given the environment settings, returns a handler function
 * that creates a project in its own directory under the work directory
 * specified in the environment
 * by importing files from an existing site.
 *
 * @param environment environment for the handler
 * @returns {Function} handler function
 */
function createFromExistingSite (environment) {
  return function (req, res) {
    const sshOptions = {
      host: req.body.host,
      username: req.body.username,
      password: req.body.password,
      privateKey: req.file ? req.file.buffer : undefined
    }

    res.writeHead(200, { 'Content-Type': 'application/json' })
    res.write(Buffer.alloc(1024, 32))
    res.write('[\n')

    const output = Writable({ objectMode: true })
    output._write = (chunk, enc, next) => {
      res.write(JSON.stringify(chunk))
      res.write(',\n')
      next()
    }

    Project.createFromExistingSite(
      environment,
      sshOptions,
      req.body.wordPressRoot,
      req.body.slug,
      output
    )
      .then((project) => {
        res.write('\n]\n')
      })
      .catch((err) => {
        console.log(err)
        res.write('\n,' + JSON.stringify(err) + ']\n')
      })
      .finally(() => {
        res.end()
      })
  }
}
