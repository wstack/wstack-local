/**
 * Wrapper for wp-cli.
 */
'use strict'

const shellEscape = require('shell-escape')
const concat = require('concat-stream')
const ssh = require('../ssh')
const dockerCompose = require('../docker-compose')

module.exports = {
  /**
   * Executes wp config get --format=json.
   *
   * @param wordPressDirectory directory where WordPress is installed
   * @param sshOptions options for SSH
   * @returns Promise<config> configuration as returned by wp-cli.
   */
  configGet: (wordPressDirectory, sshOptions) => {
    const cmd = ['wp', 'config', 'get', '--format=json', '--path=' + wordPressDirectory]
    return ssh.executeAndReturnString(sshOptions, shellEscape(cmd))
      .then((output) => {
        return JSON.parse(output)
      })
  },

  dbExport: (wordPressDirectory, sshOptions) => {
    const cmd = ['wp', 'db', 'export', '-', '--path=' + wordPressDirectory]
    return ssh.executeAndReturnStream(sshOptions, shellEscape(cmd))
  },

  dbImport: (directory, readableStream) => {
    let stdout
    const stdoutConcatStream = concat({ encoding: 'string' }, data => { stdout = data })

    return dockerCompose
      .run(
        directory,
        'php-cli', // service in docker-compose.yml
        ['wp', 'db', 'import', '-'], // command to run
        stdoutConcatStream, // stream for stdout
        readableStream // stream for stdin
      )
      .then(() => stdout)
  },

  dbQuery: (directory, sql) => {
    let stdout
    const stdoutConcatStream = concat({ encoding: 'string' }, data => { stdout = data })

    return dockerCompose
      .run(
        directory,
        'php-cli', // service in docker-compose.yml
        ['wp', 'db', 'query', sql], // command to run
        stdoutConcatStream // stream for output
      )
      .then(() => stdout)
  },

  optionGet: (directory, key, sshOptions) => {
    const cmd = ['wp', 'option', 'get', key, '--format=json', '--path=' + directory]
    return ssh.executeAndReturnString(sshOptions, shellEscape(cmd))
      .then((output) => {
        return JSON.parse(output)
      })
  },

  searchReplace: (directory, oldValue, newValue) => {
    let stdout
    const stdoutConcatStream = concat({ encoding: 'string' }, data => { stdout = data })

    return dockerCompose
      .run(
        directory,
        'php-cli', // service in docker-compose.yml
        ['wp', 'search-replace', oldValue, newValue], // command to run
        stdoutConcatStream // stream for stdout
      )
      .then(() => stdout)
  },

  coreInstall: (directory, url, title, adminUser, adminPassword, adminEmail) => {
    let stdout
    const stdoutConcatStream = concat({ encoding: 'string' }, data => { stdout = data })

    return dockerCompose
      .run(
        directory,
        'php-cli', // service in docker-compose.yml
      [ 'wp', 'core', 'install',
        `--url=${url}`, `--title=${title}`,
        `--admin_user=${adminUser}`, `--admin_password=${adminPassword}`, `--admin_email=${adminEmail}`,
        '--skip-email'
      ], // command to run
        stdoutConcatStream // stream for stdout
      )
      .then(() => stdout)
  }
}
