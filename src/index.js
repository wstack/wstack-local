'use strict'

const express = require('express')
const bodyParser = require('body-parser')
const multer = require('multer')
const project = require('./project')
const path = require('path')

const PORT = process.env.PORT || 8000
const WORK_DIR = path.resolve(process.env.WORK_DIR || '/var/work')
const HOST_WORK_DIR = process.env.HOST_WORK_DIR || WORK_DIR

const environment = {
  workDir: WORK_DIR,
  hostWorkDir: HOST_WORK_DIR
}
const multerOptions = {
  limits: {
    fileSize: 2048
  }
}

const app = express()
app.use(bodyParser.urlencoded({extended: true})) // TODO: CSRF danger! See https://fosterelli.co/dangerous-use-of-express-body-parser
app.use(bodyParser.json())

const router = express.Router()
router.post('/projects', project.createFromScratch(environment))
router.post('/import', multer(multerOptions).single('sshPrivateKeyFile'), project.createFromExistingSite(environment))

app.use('/api', router)
app.use(express.static('src/html'))

app.listen(PORT)

console.log('Running on http://localhost:' + PORT + '.')
console.log('Local working directory: ' + WORK_DIR)
console.log('Host working directory: ' + HOST_WORK_DIR)
