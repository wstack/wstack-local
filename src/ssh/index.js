/**
 * Wrapper for ssh2.
 */
'use strict'

const Promise = require('bluebird')
const SshClient = require('ssh2').Client
const concat = require('concat-stream')
const fs = require('fs-extra')
const FileFilter = require('../file-filter/FileFilter')
const Actions = require('../actions')

class SshError extends Error {
  constructor (message, code, signal, didCoreDump, description, stderr) {
    super(message)
    this.code = code
    this.signal = signal
    // noinspection JSUnusedGlobalSymbols
    this.didCoreDump = didCoreDump
    this.description = description
    this.stderr = stderr
  }
}

module.exports = {
  /**
   * Executes a command over SSH connection and returns a promise of the stdout output as a string.
   *
   * @param connectionOptions options passed to ssh2 Client.connect.
   * @param command command to execute
   * @return Promise<string> Promise of stdout output
   */
  executeAndReturnString: (connectionOptions, command) => {
    return new Promise((resolve, reject) => {
      const conn = Promise.promisifyAll(new SshClient())

      let stdoutOutput = ''
      const stdoutConcatStream = concat(
        { encoding: 'string' },
        (data) => { stdoutOutput = data }
      )

      let stderrOutput = ''
      const stderrConcatStream = concat(
        { encoding: 'string' },
        (data) => { stderrOutput = data }
      )

      conn
        .on('ready', () => {
          console.log('ssh: connection ready')
          conn.execAsync(command)
            .then((stream) => {
              stream.pipe(stdoutConcatStream)
              stream.stderr.pipe(stderrConcatStream)
              stream
                .on('close', (code, signal, didCoreDump, description) => {
                  console.log('ssh: stream close')
                  if (code === 0) {
                    resolve(stdoutOutput)
                  } else {
                    reject(new SshError(
                      'command exited with error code ' + code,
                      code,
                      signal,
                      didCoreDump,
                      description,
                      stderrOutput
                    ))
                  }
                  conn.end()
                })
                .on('end', () => {
                  console.log(`ssh: stream end`)
                })
                .on('error', () => {
                  console.log(`ssh: stream error`)
                })
            })
            .catch((err) => {
              console.log('ssh: stream error')
              reject(err)
            })
        })
        .on('banner', (message, language) => {
          console.log(`ssh: banner(${message}, ${language})`)
        })
        .on('keyboard-interactive', () => {
          console.log('ssh: keyboard-interactive')
          reject(new SshError('keyboard-interactive not supported'))
        })
        .on('change password', () => {
          console.log('ssh: change password')
          reject(new SshError('change password not supported'))
        })
        .on('continue', () => {
          console.log('ssh: continue')
        })
        .on('error', (err) => {
          console.log(`ssh: error(${err})`)
          reject(err)
        })
        .on('end', () => {
          console.log('ssh: end')
        })
        .on('close', (hadError) => {
          console.log(`ssh: close(${hadError})`)
          // If promise is already resolved, this will be ignored:
          reject(new SshError('ssh: connection closed unexpectedly'))
        })
        .connect(connectionOptions)
    })
  },

  /**
   * Executes a command over SSH connection and returns a promise of the stream of command.
   *
   * @param connectionOptions options passed to ssh2 Client.connect.
   * @param command command to execute
   * @return Promise<Channel> Promise of ssh2 channel
   */
  executeAndReturnStream: (connectionOptions, command) => {
    return new Promise((resolve, reject) => {
      const conn = Promise.promisifyAll(new SshClient())

      conn
        .on('ready', () => {
          conn.execAsync(command).then((stream) => {
            stream
              .on('end', () => {
                console.log(`ssh: stream end`)
              })
              .on('close', () => {
                console.log(`ssh: stream close`)
                conn.end()
              })
              .on('error', () => {
                console.log(`ssh: stream error`)
              })
            resolve(stream)
          }).catch(reject)
        })
        .on('banner', (message, language) => {
          console.log(`ssh: banner(${message}, ${language})`)
        })
        .on('keyboard-interactive', () => {
          console.log('ssh: keyboard-interactive')
          reject(new SshError('keyboard-interactive not supported'))
        })
        .on('change password', () => {
          console.log('ssh: change password')
          reject(new SshError('change password not supported'))
        })
        .on('continue', () => {
          console.log('ssh: continue')
        })
        .on('error', (err) => {
          console.log(`ssh: error(${err})`)
          reject(err)
        })
        .on('end', () => {
          console.log('ssh: end')
        })
        .on('close', (hadError) => {
          console.log(`ssh: close(${hadError})`)
          // If promise is already resolved, this will be ignored:
          reject(new SshError('ssh: connection closed unexpectedly'))
        })
        .connect(connectionOptions)
    })
  },

  /**
   * Imports files from remote host to a local directory.
   *
   * @param connectionOptions options passed to ssh2 Client.connect.
   * @param remoteDirectory directory on remote host to import
   * @param localDirectory directory on container where to import
   * @param filters file filters
   * @param output object output stream for progress events
   */
  importFiles: (connectionOptions, remoteDirectory, localDirectory, filters = '', output) => {
    return Promise.using(connect(connectionOptions, output), (conn) => {
      return conn
        .sftpAsync()
        .then((sftp) => {
          console.log(`sftp: ready`)
          sftp = Promise.promisifyAll(sftp)
          return importDirectory(sftp, remoteDirectory, localDirectory, '', null, FileFilter.fromString(filters), output)
        })
    })
  },

  SshError: SshError
}

function connect (connectionOptions, output) {
  return new Promise((resolve, reject) => {
    const conn = Promise.promisifyAll(new SshClient())
    const { host, username } = connectionOptions

    output.write(Actions.startTask('connect', { host, username }))

    conn
      .on('ready', () => {
        output.write(Actions.finishTask('connect'))
        resolve(conn)
      })
      .on('banner', (message, language) => {
        console.log(`ssh: banner(${message}, ${language})`)
      })
      .on('keyboard-interactive', () => {
        console.log('ssh: keyboard-interactive')
        reject(new SshError('keyboard-interactive not supported'))
      })
      .on('change password', () => {
        console.log('ssh: change password')
        reject(new SshError('change password not supported'))
      })
      .on('continue', () => {
        console.log('ssh: continue')
      })
      .on('error', (err) => {
        console.log(`ssh: error(${err})`)
        reject(err)
      })
      .on('end', () => {
        console.log('ssh: end')
      })
      .on('close', (hadError) => {
        console.log(`ssh: close(${hadError})`)
        // If promise is already resolved, this will be ignored:
        reject(new SshError('ssh: connection closed unexpectedly'))
      })
      .connect(connectionOptions)
  })
  .catch(err => {
    output.write(Actions.taskFailed('connect', err))
    throw (err)
  })
  .disposer(connection => { connection && connection.end() })
}

function remoteIsDirectory (remoteFileInfo) {
  return (remoteFileInfo.attrs.mode & fs.constants.S_IFMT) === fs.constants.S_IFDIR
}

function localMkdirIfNeeded (localDirectory) {
  return fs.mkdir(localDirectory)
    .then(() => {
      console.log(`Created local directory ${localDirectory}`)
    })
    .catch(fileExists, () => {
      console.log(`Local directory ${localDirectory} already exists`)
    })
}

function fileExists (err) {
  return err.code === 'EEXIST'
}

function importDirectory (sftp, remoteRoot, localRoot, directory, attrs, fileFilter, output) {
  const localDirectory = localRoot + directory
  const remoteDirectory = remoteRoot + directory

  return Promise
    .try(() => {
      output.write(Actions.startTask('importDirectory', {directory: remoteDirectory}))
    })
    .then(() => {
      return localMkdirIfNeeded(localDirectory)
    })
    .then(() => {
      return sftp.readdirAsync(remoteDirectory)
    })
    .then((remoteDirectoryListing) => {
      return Promise.map(
        remoteDirectoryListing,
        (remoteFileInfo) => {
          const file = remoteFileInfo.filename
          const filePath = directory + '/' + file
          const remoteFile = remoteDirectory + '/' + file
          const localFile = localDirectory + '/' + file // TODO: Windows?

          if (fileFilter.includes(filePath)) {
            if (remoteIsDirectory(remoteFileInfo)) {
              return importDirectory(sftp, remoteRoot, localRoot, filePath, remoteFileInfo.attrs, fileFilter, output)
            } else {
              return importFile(sftp, remoteFile, localFile, remoteFileInfo.attrs, output)
            }
          } else {
            output.write(Actions.skipFile('importFile', { file: remoteFile }))
          }
        },
        {concurrency: 1}
      )
    })
    .then(() => {
      return attrs ? fs.utimes(localDirectory, attrs.atime, attrs.mtime) : null
    })
    .then(() => {
      output.write(Actions.finishTask('importDirectory'))
    })
}

function importFile (sftp, remoteFile, localFile, attrs, output) {
  return Promise
    .try(() => {
      output.write(Actions.startTask('importFile', {file: remoteFile}))
    })
    .then(() => {
      return sftp.fastGetAsync(remoteFile, localFile)
    })
    .then(() => {
      return fs.utimes(localFile, attrs.atime, attrs.mtime)
    })
    .then(() => {
      output.write(Actions.finishTask('importFile'))
    })
}
