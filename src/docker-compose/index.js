/**
 * Wrapper for running docker-compose on Docker.
 */

'use strict'

const Promise = require('bluebird')
const Docker = require('dockerode')
const _ = require('lodash')
const concat = require('concat-stream')
const DOCKER_COMPOSE_IMAGE = 'docker/compose:1.18.0'

module.exports = {
  /**
   * Runs "docker-compose up" in the given directory.
   *
   * @param directory directory where to run it
   * @returns Promise
   */
  up: (directory) => {
    const docker = new Docker()
    const followProgress = Promise.promisify(docker.modem.followProgress)

    return docker
      .pull(DOCKER_COMPOSE_IMAGE)
      .then(followProgress)
      .then(() => {
        // Promisify not used because docker.run returns EventEmitter
        // and we might use it for something some day:
        return new Promise((resolve, reject) => {
          docker.run(
            DOCKER_COMPOSE_IMAGE, // image
            ['up', '-d'], // command
            process.stdout, // stream for output
            { // container create options
              Env: [
                'PROJECT_DIR=' + directory
              ],
              WorkingDir: '/data',
              Binds: [
                directory + ':/data',
                '/var/run/docker.sock:/var/run/docker.sock:ro'
              ]
            },
            (err, data) => { // callback
              if (err) {
                reject(err)
              }
              resolve(data)
            }
          )
        })
      })
  },

  /**
   * Runs "docker-compose run" in the given directory.
   *
   * @param directory host directory where to run it
   * @param service service to use for running
   * @param command command to run
   * @param stdout writable stream for output
   * @param stdin readable stream for input
   * @returns Promise
   */
  run: (directory, service, command, stdout, stdin) => {
    const docker = new Docker({ Promise })
    const followProgress = Promise.promisify(docker.modem.followProgress)

    return docker
      .pull(DOCKER_COMPOSE_IMAGE)
      .then(followProgress)
      .then(() => {
        return docker.createContainer({
          Image: DOCKER_COMPOSE_IMAGE,
          Cmd: ['--file=/data/docker-compose-run.yml', 'run', '--rm', service].concat(command),
          Tty: false,
          AttachStdin: !_.isUndefined(stdin),
          OpenStdin: !_.isUndefined(stdin),
          StdinOnce: true,
          WorkingDir: '/data',
          Binds: [
            directory + ':/data',
            '/var/run/docker.sock:/var/run/docker.sock:ro'
          ],
          Env: [
            'PROJECT_DIR=' + directory
          ]
        })
      })
      .then(container => {
        return container
          .attach({
            stream: true,
            stdin: !_.isUndefined(stdin),
            stdout: true,
            stderr: true,
            hijack: true
          })
          .then(stream => {
            let stderrOutput = ''
            const stderr = concat(
              { encoding: 'string' },
              (data) => { stderrOutput = data }
            )
            if (stdin) {
              stdin.pipe(stream)
            }

            docker.modem.demuxStream(stream, stdout, stderr)
            return container
              .start()
              .then(() => {
                return container.wait()
              })
              .then(result => {
                stdout.end()
                stderr.end()
                if (result.StatusCode !== 0) {
                  throw new Error(stderrOutput)
                }
              })
              .finally(() => container.remove())
          })
      })
  }
}
