/**
 * Tests for learning how to run docker-compose programmatically using Docker.
 */

'use strict'
/* eslint-env mocha */

// Imports:
const chai = require('chai')
const expect = chai.expect
const Promise = require('bluebird')
const concat = require('concat-stream')

const fs = Promise.promisifyAll(require('fs-extra'))
const path = require('path')
const tmp = Promise.promisifyAll(require('tmp'))

const Docker = require('dockerode')

describe('Run Docker Compose using Docker', function () {
  beforeEach('Create a directory where to run docker-compose and copy required files there', function () {
    const fixture = this

    return tmp.dirAsync({
      dir: '/tmp', // The default MacOS location is not allowed in Docker by default. TODO: windows
      prefix: 'learning-docker-compose-',
      unsafeCleanup: true
    })
        .then((dir, cleanupCallback) => {
          fixture.workDir = dir
          fixture.cleanupCallback = cleanupCallback

          return fs.copyAsync(
                path.join('test-integration', 'docker-compose', 'docker-compose-yml-for-learning-docker-compose.yml'),
                path.join(dir, 'docker-compose.yml'),
                { errorOnExist: true }
            ).then(() => {
              fs.copyAsync(
                    path.join('test-integration', 'docker-compose', 'dot-env-for-learning-docker-compose'),
                    path.join(dir, '.env'),
                    { errorOnExist: true }
                )
            })
        })
  })

  beforeEach('Create Docker API instance', function () {
    const fixture = this
    fixture.docker = new Docker()
  })

  it('should run docker-compose with Docker in the created directory', function (done) {
    const fixture = this
    const docker = fixture.docker

        // Promisify not used because docker.run returns EventEmitter
        // and we might use it for something some day.

    this.timeout(2 * 60 * 1000)
    setTimeout(done, 2 * 60 * 1000)

    const stdoutConcatStream = concat({ encoding: 'string' }, stdoutCaptured)
    let stdoutOutput

    docker.run(
            'docker/compose:1.8.1', // image
            'up', // command
            [ stdoutConcatStream, process.stderr ], // streams for output
      {  // container create options
        Env: ['MY_ENV_VARIABLE=myEnvVariableValue'],
        WorkingDir: '/data',
        Binds: [
          fixture.workDir + ':/data',
          '/var/run/docker.sock:/var/run/docker.sock:ro'
        ],
        Tty: false
      },
            dockerComposeUpFinished // callback
        )

    function stdoutCaptured (output) {
      stdoutOutput = output
    }

    function dockerComposeUpFinished (err, data) {
      if (err) { done(err) }

      expect(data.StatusCode).to.equal(0)
      expect(stdoutOutput).to.contain('MY_ENV_VARIABLE=myEnvVariableValue')
      expect(stdoutOutput).to.contain('DOT_ENV_VARIABLE=dotEnvVariableValue')

      done()
    }
  })

  afterEach(function () {
    const fixture = this
    if (fixture.cleanupCallback) {
      fixture.cleanupCallback()
    }
  })
})
