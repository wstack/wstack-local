'use strict'
/* eslint-env mocha */

const chai = require('chai')
const expect = chai.expect
const concat = require('concat-stream')
const path = require('path')
const stringToStream = require('string-to-stream')

const dockerCompose = require('../../src/docker-compose')

describe('#run', function () {
  this.timeout(10000)

  it('with stdin', function () {
    const input = stringToStream('beep')

    let outputString
    const output = concat({ encoding: 'string' }, data => { outputString = data })

    return dockerCompose
      .run(path.join(process.cwd(), 'test-integration', 'docker-compose'), 'alpine', ['md5sum'], output, input)
      .then(() => {
        return expect(outputString).to.equal('1284e53a168a5ad955485a7c83b10de0  -\n')
      })
  })

  it('without stdin', function () {
    let outputString
    const output = concat({ encoding: 'string' }, data => { outputString = data })

    return dockerCompose
      .run(path.join(process.cwd(), 'test-integration', 'docker-compose'), 'alpine', ['echo', 'foo'], output)
      .then(() => {
        return expect(outputString).to.equal('foo\n')
      })
  })
})
