/**
 * Tests for learning how to programmatically use Yeoman.
 */

'use strict'
/* eslint-env mocha */

// Imports:
const chai = require('chai')
const expect = chai.expect
const Promise = require('bluebird')

const fs = Promise.promisifyAll(require('fs-extra'))
const os = require('os')
const path = require('path')
const moment = require('moment')

const yeoman = require('yeoman-environment')

describe('Run a Yeoman generator', () => {
  const workDir = path.join(os.tmpdir(), 'yeoman-generated-project-' + moment().format('YYYYMMDDTHHmmss'))

  beforeEach('Create a directory where to run the generator', function () {
    return fs.mkdirAsync(workDir)
  })

  it('should install WordPress in the created directory', function () {
    process.chdir(workDir)

    const env = Promise.promisifyAll(yeoman.createEnv())
    env.register(require.resolve('generator-wysiwygoy-wordpress'), 'wysiwygoy:wordpress')
    return env.runAsync('wysiwygoy:wordpress').then(() => {
      const indexPhp = path.join(workDir, 'htdocs', 'index.php')
      const wpConfigPhp = path.join(workDir, 'htdocs', 'wp-config.php')
      const wpContent = path.join(workDir, 'htdocs', 'wp-content')

      return Promise.all([
        fs.statAsync(indexPhp).then((stat) => {
          return expect(stat.isFile()).to.be.true
        }),
        fs.statAsync(wpConfigPhp).then((stat) => {
          return expect(stat.isFile()).to.be.true
        }),
        fs.statAsync(wpContent).then((stat) => {
          return expect(stat.isDirectory()).to.be.true
        })
      ])
    })
  })

  afterEach('Clean up the directory created for the generator', function () {
    return fs.removeAsync(workDir)
  })
})
