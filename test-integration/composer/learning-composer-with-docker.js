/**
 * Tests for learning how to programmatically use Docker.
 */

'use strict'
/* eslint-env mocha */

// Imports:
const chai = require('chai')
const dirtyChai = require('dirty-chai')
const expect = chai.expect
const Promise = require('bluebird')

const fs = Promise.promisifyAll(require('fs-extra'))
const path = require('path')
const tmp = Promise.promisifyAll(require('tmp'))

const Docker = require('dockerode')

chai.use(dirtyChai)

describe('Run Composer using Docker', function () {
  beforeEach('Create a directory where to run Composer and composer.json there', function () {
    const fixture = this

    return tmp.dirAsync({
      dir: '/tmp', // The default MacOS location is not allowed in Docker by default. TODO: windows
      prefix: 'learning-composer-with-docker-',
      unsafeCleanup: true
    })
        .then((dir, cleanupCallback) => {
          fixture.workDir = dir
          fixture.cleanupCallback = cleanupCallback

          return fs.copyAsync(
                path.join('test-integration', 'composer', 'composer-json-for-learning-composer-with-docker.json'),
                path.join(dir, 'composer.json'),
                { errorOnExist: true }
            )
        })
  })

  beforeEach('Create Docker API instance', function () {
    const fixture = this
    fixture.docker = new Docker()
  })

  it('should run Composer with Docker in the created directory', function (done) {
    const fixture = this
    const docker = fixture.docker

        // Promisify not used because docker.run returns EventEmitter
        // and we might use it for something some day.

    this.timeout(2 * 60 * 1000)
    setTimeout(done, 2 * 60 * 1000)

    function runComposerInstall () {
      docker.run(
                'composer/composer:alpine', // image
                'install', // command
                process.stdout, // stream for output
                { Binds: [fixture.workDir + ':/app'] }, // container create options
                composerInstallFinished // callback
            )
    }

    function composerInstallFinished (err, data) {
      if (err) { done(err) }

      expect(data.StatusCode).to.equal(0)

      const composerLock = path.join(fixture.workDir, 'composer.lock')
      fs.stat(composerLock, (err, stat) => {
        if (err) { done(err) }

                // noinspection BadExpressionStatementJS
        expect(stat.isFile()).to.be.true()
        done()
      })
    }

    runComposerInstall()
  })

  afterEach(function () {
    const fixture = this
    if (fixture.cleanupCallback) {
      fixture.cleanupCallback()
    }
  })
})
