/**
 * Tests for learning how to use ssh2 node module.
 */

'use strict'
/* eslint-env mocha */

const SshClient = require('ssh2').Client
const Promise = require('bluebird')
const fs = require('fs-extra')
const os = require('os')
const moment = require('moment')
const path = require('path')
const chai = require('chai')
const chaiAsPromised = require('chai-as-promised')
chai.use(chaiAsPromised)
const expect = chai.expect

describe('get directory listing over sftp', function () {
  it('gets directory listing from remote server', function (done) {
    const conn = Promise.promisifyAll(new SshClient())

    conn.on('ready', () => {
      conn.sftpAsync()
        .then((sftp) => {
          sftp = Promise.promisifyAll(sftp)
          return sftp.readdirAsync('public_html')
        })
        .then((list) => {
          console.dir(list)
          conn.end()
        })
        .catch(done)
    })
      .on('end', () => {
        done()
      })
      .on('error', (err) => {
        done(err)
      })
      .connect({
        host: 'cls.wysiwyg.fi',
        username: 'wpdemo',
        privateKey: fs.readFileSync('/Users/jarno/.ssh/id_rsa')
      })
  })
})

describe('get file over sftp', function () {
  const workDir = path.join(os.tmpdir(), 'learning-sftp-' + moment().format('YYYYMMDDTHHmmss'))
  console.log(`workDir: ${workDir}`)

  before('Create a directory', function () {
    return fs.mkdir(workDir)
  })

  it('gets a file from remote server', function (done) {
    const conn = Promise.promisifyAll(new SshClient())

    conn
      .on('ready', () => {
        conn.sftpAsync()
          .then((sftp) => {
            sftp = Promise.promisifyAll(sftp)
            return sftp.fastGetAsync('public_html/wp-config.php', path.join(workDir, 'wp-config.php'))
          })
          .then(() => {
            expect(fs.pathExists(path.join(workDir, 'wp-config.php'))).to.eventually.be.true.notify(done)
          })
          .catch(done)
      })
      .on('error', (err) => {
        done(err)
      })
      .on('close', () => {
        done(new Error('Connection closed unexpectedly'))
      })
      .connect({
        host: 'cls.wysiwyg.fi',
        username: 'wpdemo',
        privateKey: fs.readFileSync('/Users/jarno/.ssh/id_rsa')
      })
  })

  after('Clean up the directory created for the test', function () {
    // return fs.remove(workDir)
  })
})
