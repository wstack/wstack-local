/**
 * Tests for learning how to run wp-cli using docker-compose.
 */

'use strict'
/* eslint-env mocha */

// Imports:
const chai = require('chai')
const expect = chai.expect
const dockerCompose = require('../../src/docker-compose')
const concat = require('concat-stream')

describe('Run wp-cli using docker-compose', function () {
  it('should get wp-config as json', function () {
    this.timeout(0)

    let config
    const stdoutConcatStream = concat({ encoding: 'string' }, (data) => { config = JSON.parse(data) })

    return dockerCompose.run(
      'test2', // slug
      '/Users/jarno/work/test2', // directory
      'php-cli', // service in docker-compose.yml
      ['wp', 'config', 'get', '--format=json', '--path=/site/public'], // command to run
      stdoutConcatStream // stream for output
    ).then(() => {
      console.log(config)
      expect(config).to.be.an('array').that.deep.includes({ key: 'table_prefix', 'value': 'wp_', type: 'variable' })
    })
  })
})
