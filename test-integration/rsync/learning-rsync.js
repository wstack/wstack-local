/**
 * Tests for learning how to use node-rsync.
 */

'use strict'
/* eslint-env mocha */

// Imports:
const chai = require('chai')
const dirtyChai = require('dirty-chai')
const expect = chai.expect
const Promise = require('bluebird')

const fs = Promise.promisifyAll(require('fs-extra'))
const os = require('os')
const path = require('path')
const moment = require('moment')

const rsync = Promise.promisify(require('rsyncwrapper'), { multiArgs: true })

chai.use(dirtyChai)

describe('rsync from remote server to local directory', () => {
  const workDir = path.join(os.tmpdir(), 'learning-rsync-' + moment().format('YYYYMMDDTHHmmss'))

  beforeEach('Create a directory where to copy files', function () {
    return fs.mkdirAsync(workDir)
  })

  it('should copy files from remote server', function () {
    return rsync({
      src: 'jarnoan@cls.wysiwyg.fi:test/learning-rsync/',
      dest: workDir,
      recursive: true,
      ssh: true
    }).then(
      function (result) {
        console.log('STDOUT: ' + result[0])
        console.log('STDERR: ' + result[1])
        console.log('CMD: ' + result[2])

        const fileTxt = path.join(workDir, 'file.txt')

        return fs.statAsync(fileTxt).then(
          function (stat) {
            expect(stat.isFile()).to.be.true()
          }
        )
      }
    )
  })

  afterEach('Clean up the directory created for the test', function () {
    // return fs.removeAsync(workDir)
  })
})
