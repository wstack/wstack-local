User interface of Wysiwyg Oy Development Environment
====================================================

Running locally
---------------

Running locally with Node:

    npm start
    
Running locally with Nodemon will restart the server when any file is changed:

    nodemon

These will start the server in port 8000 and working directory `/var/work` by default. 
To use another port or working directory, set environment variables `PORT` or `WORK_DIR`, respectively.

    WORK_DIR=/Users/jarno/work nodemon

How it works
------------

The Docker image is based on the official Node.js Docker image.
